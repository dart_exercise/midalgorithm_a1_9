/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.midalgo_a1_9;

import java.util.Scanner;

/**
 *
 * @author WIP
 */
public class ExamA1_9 {

    public static void main(String[] args) {
        long start, stop;
        start = System.nanoTime();
        Scanner kb = new Scanner(System.in);
        String str = kb.next();
        
        //convert String to Integer
        int num = Integer.parseInt(str);
        

        stop = System.nanoTime();
        System.out.println(num);
        System.out.println("time = " + (stop - start) * 1E-9  + "secs.");
    }

}
